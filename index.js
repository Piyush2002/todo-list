
let taskCountSpan = document.getElementById("taskCount-span");
let completedSpan = document.getElementById("task-completed");
let taskCount = 0;
let completedTask = 0;


let todoListArray = JSON.parse(localStorage.getItem('todos')) || [];
document.addEventListener("DOMContentLoaded", function () {
    loadTodos();

    updateTaskCount();

})




const input_user = document.getElementById("tasks-data");

input_user.addEventListener("change", function () {
    addTodo();
})


function addTodo() {
    const inputElement = document.getElementById('tasks-data');

    if (inputElement.value.trim() !== '') {
        const newTodo = {
            id: new Date().getTime(),
            text: inputElement.value.trim(),
            completed: false
        };

        todoListArray.push(newTodo);
        updateLocalStorage();
        loadTodos();
        inputElement.value = '';
    }
    updateTaskCount();
}

function loadTodos() {
    const todoListElement = document.getElementById('task');
    todoListElement.innerHTML = '';

    todoListArray.forEach(todo => {

        const li = document.createElement('li');
        li.className = "items";
        li.innerHTML = `<input type="checkbox" class="check" ${todo.completed ? 'checked' : ''}>
                    <p class="li-text" id="p-data">${todo.text}</p>
                    <button class="edit">Edit</button>
                    <i class="fa-solid fa-xmark" onclick=deleteTodo(${todo.id}) id="cross"></i>`;

        todoListElement.appendChild(li);

        const checkbox = li.querySelector('.check');
        let para = li.querySelector('.li-text');

        checkbox.addEventListener('change', function () {

            todo.completed = checkbox.checked;
            para.style.textDecoration = checkbox.checked ? "line-through" : "none";

            updateLocalStorage();
            updateTaskCount();

        });

        para.style.textDecoration = todo.completed ? "line-through" : "none";
        editTask(li, todo);
    });



    // updateTaskCount();
}

function deleteTodo(todoId) {
    const indexToDelete = todoListArray.findIndex(todo => todo.id === todoId);

    if (indexToDelete !== -1) {
        todoListArray.splice(indexToDelete, 1);
        updateLocalStorage();
        loadTodos();
    }
    updateTaskCount();
}


function editTask(li, todo) {
    let para = li.querySelector('.li-text');
    let editBtn = li.querySelector('.edit');

    para.addEventListener("dblclick", function () {
        para.setAttribute("contenteditable", "true");
        para.focus();
    });

    editBtn.addEventListener("click", function () {
        para.setAttribute("contenteditable", "true");
        para.focus();
    });

    para.addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            para.setAttribute("contenteditable", "false");
            todo.text = para.textContent.trim();
            updateLocalStorage();
            loadTodos();
        }
    });


    para.addEventListener("blur", function () {
        para.setAttribute("contenteditable", "false");
        todo.text = para.textContent.trim();
        updateLocalStorage();
        loadTodos();
    });
}



function updateTaskCount() {
    completedTask = todoListArray.filter(todo => todo.completed).length;
    taskCount = todoListArray.length - completedTask;
    completedSpan.innerHTML = `Completed Tasks: ${completedTask}`;
    taskCountSpan.innerHTML = `Task To Done: ${taskCount}`;
}


function updateLocalStorage() {
    localStorage.setItem('todos', JSON.stringify(todoListArray));
}


function showAllTasks() {
    const taskItems = document.querySelectorAll('.items');

    taskItems.forEach(element => {

        element.style.display = "flex";

    });
}


function showActiveTasks() {
    const taskItems = document.querySelectorAll('.items');

    taskItems.forEach(element => {
        let checkbox = element.querySelector('.check');
        if (!checkbox.checked) {
            element.style.display = "flex";
        } else {
            element.style.display = "none";
        }
    });
}

function showCompletedTasks() {
    const taskItems = document.querySelectorAll('.items');

    taskItems.forEach(element => {
        let checkbox = element.querySelector('.check');
        if (checkbox.checked) {
            element.style.display = "flex";
        } else {
            element.style.display = "none";
        }
    });
}

function deleteCompTask() {
    todoListArray = todoListArray.filter(todo => !todo.completed);
    updateLocalStorage();
    loadTodos();
    updateTaskCount();
}

loadTodos();